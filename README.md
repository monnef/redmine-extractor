Given user name, URL and password it gets hours per month and prints in JSON.

Example output:
```json
[
    {
        "month": 9,
        "year": 2022,
        "hours": 4
    }
]
```

# Install

## Local package globally

```sh
$ git clone https://gitlab.com/monnef/redmine-extractor.git
$ cd readmine-extractor
$ npm i
$ npm link
$ redmine-extractor -h
Redmine Extractor by monnef.
Extracts worked hours per month from Redmine.
Usage: redmine-extractor <--user USER_NAME> <--url URL_TO_REDMINE_INSTANCE> [--no-quit]
Password is read from stdin.
$
```

# License

GPL 3+
