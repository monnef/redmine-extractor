#!/usr/bin/env node

const { Builder, Browser, By, Key, until } = require('selenium-webdriver');
const { stdin, stdout, exit } = require('process');
const fs = require('fs');
const yargs = require('yargs');
const { pipe } = require('ts-opt');
const { init, tail, zip, map } = require('lodash/fp');

const timeEntriesUrl = '/time_entries/report?utf8=%E2%9C%93&criteria%5B%5D=project&f%5B%5D=spent_on&op%5Bspent_on%5D=*&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=me&f%5B%5D=&c%5B%5D=project&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours&columns=month&criteria%5B%5D=';

const printHelpAndExit = (exitCode = 0) => {
    console.log('Redmine Extractor by monnef.\nExtracts worked hours per month from Redmine.');
    console.log('Usage: redmine-extractor <--user USER_NAME> <--url URL_TO_REDMINE_INSTANCE> [--no-quit]');
    console.log('Password is read from stdin.');
    exit(exitCode);
}

const getConfig = async() => {
    const argv = yargs.argv;
    const debug = Boolean(argv.d || argv.debug);
    if (debug) console.log(JSON.stringify(argv));
    if (argv.h || argv.help) { printHelpAndExit(); }
    const username = argv.user || argv.username;
    const urlPrefix = argv.url || argv.urlPrefix;
    if (!username) {
        console.error('Missing username, use --user.')
        printHelpAndExit(1);
    }
    if (!urlPrefix) {
        console.error('Missing url prefix, use --url.')
        printHelpAndExit(1);
    }
    let password;
    while (password === undefined) {
        try {
            password = fs.readFileSync(0, 'utf-8').trim();
        } catch (e) {
            if (e.code === 'EAGAIN') continue;
            throw e;
        }
    }
    if (!password) {
        console.error('Missing password (stdin).')
        printHelpAndExit(2);
    }
    return { username, urlPrefix, password, debug };
};

const doSanityCheck = async driver => {
    const failed = reason => {
        console.error('Sanity check failed. The page is most likely not a Redmine instance. ' + reason);
        exit(5);
    }
    try {
        if (!(await driver.getTitle()).includes('Redmine')) {
            failed('Failed to find a Redmine title.');
        }
    } catch (e) {
        failed('Failed to locate element or get text:\n' + e.toString());
    }
};

const logIn = async(driver, cfg) => {
    await driver.get(cfg.urlPrefix + '/login');
    await doSanityCheck(driver);
    await driver.findElement(By.id('username')).sendKeys(cfg.username);
    await driver.findElement(By.id('password')).sendKeys(cfg.password);
    await driver.findElement(By.css('#login-form > form')).submit();
    await driver.wait(until.urlContains('my/page'));
    await driver.get(cfg.urlPrefix + timeEntriesUrl);
};

const extractRawData = async driver => {
    const tableEl = await driver.findElement(By.id('time-report'));
    const monthYearEls = await tableEl.findElements(By.css('thead th'));
    const monthYears = pipe(
        await Promise.all(monthYearEls.map(x => x.getText())),
        init,
        tail,
    );
    const totalTimeEls = await driver.findElements(By.css('tbody tr.total td'));
    const totalTimes = pipe(
        await Promise.all(totalTimeEls.map(x => x.getText())),
        init,
        tail,
    );
    return { monthYears, totalTimes };
};

const processData = (monthYears, totalTimes) => pipe(
    zip(monthYears, totalTimes),
    map(([monthYear, hours]) => {
        const [year, month] = monthYear.split('-').map(Number);
        return { month, year, hours: Number(hours) };
    }),
);

const main = async() => {
    const cfg = await getConfig();
    cfg.logDebug = (...args) => {
        if (cfg.debug) console.log('[Debug]', ...args);
    };
    const driver = await new Builder().forBrowser(Browser.CHROME).build();
    try {
        await logIn(driver, cfg);
        const { monthYears, totalTimes } = await extractRawData(driver);
        const res = processData(monthYears, totalTimes);
        console.log(JSON.stringify(res, null, 4));
    } catch (e) {
        console.error(e);
    } finally {
        if (!yargs.argv.noQuit) await driver.quit();
    }
};

main();
